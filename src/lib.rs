//! ## About
//! The [tinylog](https://codeberg.org/bacardi55/gemini-tinylog-rfc) format is a minimal
//! microblog format for the small web (Gemini, Spartan etc). This library implements a
//! set of types useful for working with Tinylogs which can be readily converted to and
//! from strings. This would be useful for automating adding entries or possibly in an
//! aggregator type application.
//! 
//! ## Usage
//! Parsing a [`Tinylog`] from a [`String`] and back again
//! ```
//! # use tinylog_gmi::Time;
//! # #[derive(Clone, Debug)]
//! # struct NaiveTime {
//! #     year: u32,
//! #     month: u32,
//! #     day: u32,
//! #     hour: u32,
//! #     minute: u32,
//! #     tz: String,
//! # }
//! #
//! # impl Time for NaiveTime {
//! #    fn year(&self) -> u32 {
//! #        self.year
//! #    }
//! #    fn month(&self) -> u32 {
//! #        self.month
//! #    }
//! #    fn day(&self) -> u32 {
//! #        self.day
//! #    }
//! #    fn hour(&self) -> u32 {
//! #        self.hour
//! #    }
//! #    fn minute(&self) -> u32 {
//! #        self.minute
//! #    }
//! #    fn tz(&self) -> String {
//! #        self.tz.clone()
//! #    }
//! #    fn from_parts(year: u32, month: u32, day: u32, hour: u32, minute: u32, tz: String) -> Self {
//! #        Self {
//! #            year,
//! #            month,
//! #            day,
//! #            hour,
//! #            minute,
//! #            tz,
//! #        }
//! #    }
//! # }
//! use tinylog_gmi::Tinylog;
//! 
//! const TL: &'static str = include_str!("../test/tinylog.gmi");
//! let log = TL.parse::<Tinylog<NaiveTime>>().unwrap();
//! assert_eq!(log.entries.len(), 2);
//! assert_eq!(log.to_string(), TL);
//! ```

use std::{
    collections::BTreeMap,
    fmt::{self, Write},
    str::FromStr,
};

/// An item which represents a moment in time with minute precision
pub trait Time: Clone + Sized {
    fn year(&self) -> u32;
    fn month(&self) -> u32;
    fn day(&self) -> u32;
    fn hour(&self) -> u32;
    fn minute(&self) -> u32;
    fn tz(&self) -> String;
    /// The datatype must be able to be instantiated from the given parameters
    fn from_parts(year: u32, month: u32, day: u32, hour: u32, minutes: u32, tz: String) -> Self;

    /// Returns Some(Self) if the given line is a valid Tinylog entry heading
    fn parse_heading(line: &str) -> Option<Self> {
        if let Some(s) = line.strip_prefix("##").map(|s| s.trim()) {
            let Some((date, rem)) = s.split_once(' ') else {
            return None;
        };
            let Some((time, tz)) = rem.split_once(' ') else {
            return None;
        };
            if date.len() != 10 || time.len() != 5 {
                return None;
            }
            let Some((hours, minutes)) = time.split_once(':') else {
            return None;
        };
            let Ok(hours) = hours.parse::<u32>() else {
            return None;
        };
            let Ok(minutes) = minutes.parse::<u32>() else {
            return None;
        };
            let mut split_date = date.splitn(3, '-');
            let Some(Ok(year)) = split_date.next().map(|y| y.parse::<u32>()) else {
            return None;
        };
            let Some(Ok(month)) = split_date.next().map(|y| y.parse::<u32>()) else {
            return None;
        };
            let Some(Ok(day)) = split_date.next().map(|d| d.parse::<u32>()) else {
            return None;
        };
            return Some(Self::from_parts(
                year,
                month,
                day,
                hours,
                minutes,
                tz.to_string(),
            ));
        }
        None
    }
}

/// A Tinylog entry
#[derive(Clone, Debug)]
pub struct Entry<T: Time> {
    /// The date and time that the entry was made
    pub datetime: T,
    /// The text body of the entry
    pub body: String,
}

impl<T: Time> fmt::Display for Entry<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "## {}-{:02}-{:02} {:02}:{:02} {}\n{}",
            self.datetime.year(),
            self.datetime.month(),
            self.datetime.day(),
            self.datetime.hour(),
            self.datetime.minute(),
            self.datetime.tz(),
            self.body,
        )
    }
}

impl<T: Time> Entry<T> {
    /// Prints the date and time of the entry without the "##" prefix
    fn print_date(&self) -> String {
        format!(
            "{}-{:02}-{:02} {:02}:{:02} {}",
            self.datetime.year(),
            self.datetime.month(),
            self.datetime.day(),
            self.datetime.hour(),
            self.datetime.minute(),
            self.datetime.tz(),
        )
    }
}

/// A complete tinylog contains the header appearing above all entries and a
/// collection of entries
/// 
/// ## Usage
/// Parsing a [`Tinylog`] from a [`String`] and back again
/// ```
/// # use tinylog_gmi::Time;
/// # #[derive(Clone, Debug)]
/// # struct NaiveTime {
/// #     year: u32,
/// #     month: u32,
/// #     day: u32,
/// #     hour: u32,
/// #     minute: u32,
/// #     tz: String,
/// # }
/// # impl Time for NaiveTime {
/// #    fn year(&self) -> u32 {
/// #        self.year
/// #    }
/// #    fn month(&self) -> u32 {
/// #        self.month
/// #    }
/// #    fn day(&self) -> u32 {
/// #        self.day
/// #    }
/// #    fn hour(&self) -> u32 {
/// #        self.hour
/// #    }
/// #    fn minute(&self) -> u32 {
/// #        self.minute
/// #    }
/// #    fn tz(&self) -> String {
/// #        self.tz.clone()
/// #    }
/// #    fn from_parts(year: u32, month: u32, day: u32, hour: u32, minute: u32, tz: String) -> Self {
/// #        Self {
/// #            year,
/// #            month,
/// #            day,
/// #            hour,
/// #            minute,
/// #            tz,
/// #        }
/// #    }
/// # }
/// use tinylog_gmi::Tinylog;
/// 
/// const TL: &'static str = include_str!("../test/tinylog.gmi");
/// let log = TL.parse::<Tinylog<NaiveTime>>().unwrap();
/// assert_eq!(log.entries.len(), 2);
/// assert_eq!(log.to_string(), TL);
/// ```
#[derive(Clone, Debug)]
pub struct Tinylog<T: Time> {
    pub head: String,
    pub entries: BTreeMap<String, Entry<T>>,
}

impl<T: Time> FromStr for Tinylog<T> {
    type Err = fmt::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s.lines();
        let mut head = String::new();
        let mut entries = BTreeMap::new();
        let mut entry: Option<Entry<T>> = None;
        for l in lines {
            if let Some(datetime) = Time::parse_heading(l) {
                if let Some(ref e) = entry {
                    entries.insert(e.print_date(), e.clone());
                }
                entry = Some(Entry {
                    datetime,
                    body: String::new(),
                });
            } else if let Some(ref mut e) = entry {
                writeln!(e.body, "{l}")?;
            } else {
                writeln!(head, "{l}")?;
            }
        }
        if let Some(e) = entry {
            entries.insert(e.print_date(), e);
        }
        Ok(Self { head, entries })
    }
}

impl<T: Time> fmt::Display for Tinylog<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.head)?;
        self.entries
            .iter()
            .rev()
            .try_for_each(|(_k, e)| write!(f, "{e}"))
    }
}

impl<T: Time> Tinylog<T> {
    /// Inserts a new entry into the Tinylog
    pub fn insert(&mut self, entry: Entry<T>) -> Option<Entry<T>> {
        self.entries.insert(entry.print_date(), entry)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TL: &'static str = include_str!("../test/tinylog.gmi");
    const TL2: &'static str = include_str!("../test/tinylog2.gmi");

    #[derive(Clone, Debug)]
    struct Foo {
        year: u32,
        month: u32,
        day: u32,
        hour: u32,
        minute: u32,
        tz: String,
    }

    impl Time for Foo {
        fn year(&self) -> u32 {
            self.year
        }

        fn month(&self) -> u32 {
            self.month
        }

        fn day(&self) -> u32 {
            self.day
        }

        fn hour(&self) -> u32 {
            self.hour
        }

        fn minute(&self) -> u32 {
            self.minute
        }

        fn tz(&self) -> String {
            self.tz.clone()
        }

        fn from_parts(year: u32, month: u32, day: u32, hour: u32, minute: u32, tz: String) -> Self {
            Self {
                year,
                month,
                day,
                hour,
                minute,
                tz,
            }
        }
    }

    fn new_entry() -> Entry<Foo> {
        Entry {
            datetime: Foo {
                year: 2023,
                month: 06,
                day: 03,
                hour: 23,
                minute: 47,
                tz: "EST".to_string(),
            },
            body: String::from("42 is the answer!\n"),
        }
    }

    #[test]
    fn parse_tl() {
        let log: Tinylog<Foo> = TL.parse().unwrap();
        assert_eq!(log.entries.len(), 2);
        let e1 = log.entries.get("2023-06-03 20:35 EST").unwrap();
        assert_eq!(
            e1.body,
            "This was a couple hours ago. My how time flies.\n\n"
        );
    }

    #[test]
    fn print_tl() {
        let log: Tinylog<Foo> = TL.parse().unwrap();
        let log = log.to_string();
        assert_eq!(log, TL);
    }

    #[test]
    fn add_entry() {
        let mut log: Tinylog<Foo> = TL.parse().unwrap();
        let entry = new_entry();
        let _e = log.insert(entry);
        assert_eq!(log.to_string(), TL2);
    }
}
