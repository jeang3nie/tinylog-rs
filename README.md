# Tinylog
### About
The [tinylog](https://codeberg.org/bacardi55/gemini-tinylog-rfc) format is a minimal
microblog format for the small web (Gemini, Spartan etc). This library implements a
set of types useful for working with Tinylogs which can be readily converted to and
from strings. This would be useful for automating adding entries or possibly in an
aggregator type application.

### Usage
Parsing a `Tinylog` from a `String`
```Rust
// Assume some type `T` which implements the `Time` trait
use tinylog::Tinylog;

const TL: &'static str = include_str!("../test/tinylog.gmi");
let log = TL.parse::<T>().unwrap();
assert_eq!(log.entries.len(), 2);
```
